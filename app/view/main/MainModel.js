Ext.define('FiddleLoad.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'Fiddle Load'
    }
});
