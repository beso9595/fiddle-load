Ext.define('FiddleLoad.view.fiddle.FiddleListController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.fiddlelist',

    listen: {
        store: {
            'fiddle': {
                load: 'onFiddleListStoreLoad'
            }
        }
    },

    onFiddleSearch: function () {
        var value = this.lookupReference('searchtextfield').getValue();
        var store = this.getView().getStore();

        if (value) {
            store.load({
                params: {
                    action: 'FiddleAction',
                    data: [{
                        filter: [{
                            operator: '=',
                            property: 'username',
                            value: value
                        }],
                        sort: [{
                            direction: 'DESC',
                            property: 'createdDate'
                        }],
                        start: 0,
                        page: 1,
                        limit: 1000000
                    }],
                    method: 'get',
                    tid: 4,
                    type: 'rpc'
                }
            });
        } else {
            store.setData([]);
        }
    },

    onFiddleListStoreLoad: function (total) {
        if (typeof total === 'number') {
            this.getViewModel().set('bbarHtml', 'Total: <b>' + total + '</b>');
        }
    }

});
