Ext.define('FiddleLoad.store.Fiddle', {
    extend: 'Ext.data.Store',

    alias: 'store.fiddle',

    model: 'FiddleLoad.model.Fiddle',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        url: 'https://api.sencha.com/v1.0/direct/router',
        paramsAsJson: true,
        actionMethods: {
            read: 'POST'
        },
        reader: {
            type: 'json',
            rootProperty: 'result.data'
        },
        listeners: {
            exception: function () {
                Ext.toast({
                    html: 'Something has gone wrong. Please try again later.',
                    hideDuration: 1000
                });
            }
        }
    },

    listeners: {
        load: function (store, records, successful) {
            if (successful) {
                this.fireEvent('load', records.length);
            }
        }
    }
});
