Ext.define('FiddleLoad.model.Fiddle', {
    extend: 'Ext.data.Model',

    fields: [
        'createdDate',
        'deleted',
        'description',
        'dotVersion',
        'framework',
        'hasPassword',
        'id',
        'index',
        'locked',
        'modifiedDate',
        'myvote',
        'runs',
        'theme',
        'title',
        'userId',
        'username',
        'version',
        'views',
        'vote'
    ]
});
