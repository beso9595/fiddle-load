# Fiddle Load

* clone
* run `sencha app watch` in project root directory
* go to `localhost:1841`
* Sencha Cmd version: `6.2.2.36+`

### Description:
You can load [sencha fiddles](https://fiddle.sencha.com/) by `username` registered on [sencha forum](https://www.sencha.com/forum/).
There is my username by default in search field. You can try yours.

### Screenshots:

![load.jpg](https://i.imgur.com/PYJeS2W.png)

![edit.jpg](https://i.imgur.com/SSfWnLV.png)