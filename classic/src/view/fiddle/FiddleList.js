Ext.define('FiddleLoad.view.fiddle.FiddleList', {
    extend: 'Ext.grid.Panel',
    xtype: 'fiddlelist',

    requires: [
        'FiddleLoad.store.Fiddle',
        'FiddleLoad.view.fiddle.FiddleListController',
        'FiddleLoad.view.fiddle.FiddleListModel'
    ],

    controller: 'fiddlelist',
    viewModel: {
        type: 'fiddlelist'
    },

    store: {
        type: 'fiddle'
    },

    selModel: {
        mode: 'MULTI'
    },

    tbar: [
        {
            xtype: 'textfield',
            reference: 'searchtextfield',
            emptyText: 'Search by username',
            value: 'beso9595'
        },
        {
            xtype: 'button',
            text: 'Search',
            handler: 'onFiddleSearch'
        }
    ],

    bbar: [
        {
            xtype: 'tbfill'
        },
        {
            xtype: 'tbtext',
            bind: {
                html: '{bbarHtml}'
            }
        }
    ],

    columns: [
        {
            xtype: 'rownumberer'
        },
        {
            text: 'Title',
            dataIndex: 'title',
            flex: 1
        },
        {
            text: 'Description',
            dataIndex: 'description',
            flex: 1
        },
        {
            text: 'FrameWork',
            dataIndex: 'framework',
            align: 'center',
            width: 120
        },
        {
            text: 'Version',
            dataIndex: 'dotVersion',
            align: 'center',
            width: 70
        },
        {
            text: 'Theme',
            dataIndex: 'theme',
            align: 'center',
            width: 120
        },
        {
            text: 'Views',
            dataIndex: 'views',
            align: 'center',
        },
        {
            text: 'Created Date',
            dataIndex: 'createdDate',
            width: 150,
            align: 'center',
            renderer: function (value) {
                return Ext.Date.format(new Date(value), 'd.m.Y');
            }
        },
        {
            text: 'Modified Date',
            dataIndex: 'modifiedDate',
            width: 150,
            align: 'center',
            renderer: function (value) {
                return Ext.Date.format(new Date(value), 'd.m.Y');
            }
        },
        {
            dataIndex: 'id',
            sortable: false,
            menuDisabled: true,
            renderer: function (value) {
                return value ? ('<a target="_blank" href="https://fiddle.sencha.com/#view/editor&fiddle/' + value + '">Go to Fiddle</a>') : '';
            }
        }
    ]
});
