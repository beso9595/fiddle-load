Ext.define('FiddleLoad.view.main.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-main',

    requires: [
        'FiddleLoad.view.main.MainController',
        'FiddleLoad.view.main.MainModel',
        'FiddleLoad.view.fiddle.FiddleList'
    ],

    controller: 'main',
    viewModel: 'main',

    layout: 'fit',
    bodyPadding: 20,
    tabConfig: {
        plugins: 'responsive',
        responsiveConfig: {
            wide: {
                iconAlign: 'left',
                textAlign: 'left'
            },
            tall: {
                textAlign: 'center',
                width: 120
            }
        }
    },

    items: [{
        xtype: 'fiddlelist'
    }]
});
